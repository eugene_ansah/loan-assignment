package com.lendico.plangenerator.services;

import com.lendico.plangenerator.domain.BorrowerPayment;
import com.lendico.plangenerator.dto.PlanGeneratorResponse;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class PlanGeneratorService {
    private static final int DAYS_IN_MONTH = 30;
    private static final int DAYS_IN_YEAR = 360;

    public PlanGeneratorResponse generateBorrowerPlan(int period, double nominalRate, BigDecimal initialPrincipal, LocalDateTime startDate) {
        BigDecimal initialOutstandingPrincipal = initialPrincipal;
        BigDecimal annuity = calculateAnnuity(initialPrincipal, nominalRate, period);
        List<BorrowerPayment> borrowerPaymentList = planGeneratorList(initialOutstandingPrincipal, nominalRate, annuity, startDate, period);
        System.out.println("borrowerPaymentList: " + borrowerPaymentList.size());
        return PlanGeneratorResponse.of(borrowerPaymentList);
    }

    // Generate plan list for each period
    public List<BorrowerPayment> planGeneratorList(BigDecimal initialOutstandingPrincipal, double nominalRate, BigDecimal annuity, LocalDateTime startDate, int period) {
        List<BorrowerPayment> borrowerPaymentList = new ArrayList<>();
        int day = 0;
//        while (initialOutstandingPrincipal.compareTo(BigDecimal.valueOf(0)) > 0) {
        while (day < period) {
            BigDecimal interest = calculateInterest(nominalRate, initialOutstandingPrincipal);
            BigDecimal principal = calculatePrincipal(annuity, interest);
            if (day == period - 1) {
                principal = initialOutstandingPrincipal;
            }
            BorrowerPayment borrowerPayment = borrowerPayment(initialOutstandingPrincipal, annuity, principal, interest, day++, startDate);
            initialOutstandingPrincipal = borrowerPayment.getRemainingOutstandingPrincipal();
            if (borrowerPayment.getRemainingOutstandingPrincipal().compareTo(BigDecimal.valueOf(0)) <= 0) {
                borrowerPayment.setRemainingOutstandingPrincipal(BigDecimal.valueOf(0).setScale(2, BigDecimal.ROUND_HALF_EVEN));
                borrowerPaymentList.add(borrowerPayment);
                break;
            }
            borrowerPaymentList.add(borrowerPayment);
        }
        return borrowerPaymentList;
    }

    // generate plan for a month
    public BorrowerPayment borrowerPayment(BigDecimal initialOutstandingPrincipal, BigDecimal annuity, BigDecimal principal, BigDecimal interest, int day, LocalDateTime startDate) {
        BorrowerPayment borrowerPayment = new BorrowerPayment();
        borrowerPayment.setInterest(interest.setScale(2, BigDecimal.ROUND_HALF_EVEN));
        borrowerPayment.setDate(paymentDate(day, startDate));
        borrowerPayment.setPrincipal(principal.setScale(2, BigDecimal.ROUND_HALF_EVEN));
        borrowerPayment.setInitialOutstandingPrincipal(initialOutstandingPrincipal.setScale(2, BigDecimal.ROUND_HALF_EVEN));
        borrowerPayment.setBorrowerPaymentAmount(annuity.setScale(2, BigDecimal.ROUND_HALF_EVEN));
        initialOutstandingPrincipal = remainingOutStandingPrincipal(initialOutstandingPrincipal, principal);
        borrowerPayment.setRemainingOutstandingPrincipal(initialOutstandingPrincipal.setScale(2, BigDecimal.ROUND_HALF_EVEN));
        return borrowerPayment;
    }

    public BigDecimal calculateInterest(double rate, BigDecimal initialPrincipal) {
        return (BigDecimal.valueOf(rate / 100 * DAYS_IN_MONTH).multiply(initialPrincipal)).divide(BigDecimal.valueOf(DAYS_IN_YEAR), 2, RoundingMode.HALF_UP);
    }


    public BigDecimal calculatePrincipal(BigDecimal annuity, BigDecimal interest) {
        return annuity.subtract(interest);
    }


    public BigDecimal calculateAnnuity(BigDecimal initialPrincipal, double nominalRate, int period) {

        double monthlyRate = (nominalRate / 12) / 100;//12 is total months in a year and 100 is %
        return initialPrincipal.multiply(BigDecimal.valueOf((monthlyRate))).divide(BigDecimal.valueOf(1 - (Math.pow((1 + monthlyRate), -period))), 2, RoundingMode.HALF_UP);
    }


    public BigDecimal remainingOutStandingPrincipal(BigDecimal initialOutstandingPrincipal, BigDecimal principal) {
        return initialOutstandingPrincipal.subtract(principal);
    }


    public LocalDateTime paymentDate(int plusDay, LocalDateTime startDate) {
        startDate = startDate.plusDays(plusDay);
        return startDate;
    }

}

package com.lendico.plangenerator.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class PlanGeneratorRequest {
    private BigDecimal loanAmount;
    private double nominalRate;
    private int duration;
    private LocalDateTime startDate;
}

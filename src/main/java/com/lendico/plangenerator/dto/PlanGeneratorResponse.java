package com.lendico.plangenerator.dto;

import com.lendico.plangenerator.domain.BorrowerPayment;
import lombok.Data;

import java.util.List;

@Data(staticConstructor = "of")
public class PlanGeneratorResponse {
    private final List<BorrowerPayment> borrowerPayments;
}

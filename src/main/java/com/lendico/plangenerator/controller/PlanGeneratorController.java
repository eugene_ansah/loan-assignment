package com.lendico.plangenerator.controller;

import com.lendico.plangenerator.dto.PlanGeneratorRequest;
import com.lendico.plangenerator.dto.PlanGeneratorResponse;
import com.lendico.plangenerator.services.PlanGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlanGeneratorController {

    @Autowired
    PlanGeneratorService service;

    @RequestMapping("/")
    public String index() {
        return "Welcome to Lendico Plan Generator ...";
    }


    @RequestMapping("/generate-plan")
    public PlanGeneratorResponse generatePlan(@RequestBody PlanGeneratorRequest request) {
        return service.generateBorrowerPlan(request.getDuration(), request.getNominalRate(), request.getLoanAmount(), request.getStartDate());
    }
}

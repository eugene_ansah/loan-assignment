package com.lendico.plangenerator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.lendico.plangenerator.domain.BorrowerPayment;
import com.lendico.plangenerator.dto.PlanGeneratorRequest;
import com.lendico.plangenerator.services.PlanGeneratorService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EndToEndTest {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PlanGeneratorService planGeneratorService;

    @Before
    public void setUp() {
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Test
    public void calculatePrincipalTest() {
        BigDecimal interest = planGeneratorService.calculateInterest(7, BigDecimal.valueOf(3000));
        BigDecimal principal = planGeneratorService.calculatePrincipal(BigDecimal.valueOf(176.05), interest);
        Assert.assertEquals(principal.doubleValue(), 158.55, 0d);
    }

    @Test
    public void calculateInterestTest() {
        BigDecimal interest = planGeneratorService.calculateInterest(7, BigDecimal.valueOf(3000));
        Assert.assertEquals(interest.doubleValue(), 17.50, 0.1d);
    }

    @Test
    public void calculateAnnuityTest() {
        BigDecimal annuity = planGeneratorService.calculateAnnuity(BigDecimal.valueOf(3000), 7.00, 18);
        Assert.assertEquals(annuity.doubleValue(), 176.05, 0.1d);
    }

    @Test
    public void planGeneratorSizeTest() {
        LocalDateTime localDateTime = LocalDateTime.now();
        BigDecimal annuity = planGeneratorService.calculateAnnuity(BigDecimal.valueOf(3000), 7.00, 18);
        List<BorrowerPayment> borrowerPayments = planGeneratorService.planGeneratorList(BigDecimal.valueOf(3000), 7.00, annuity, localDateTime, 18);
        Assert.assertEquals(borrowerPayments.size(), 18);
    }

    @Test
    public void finalRemainingBalanceTest() {
        LocalDateTime localDateTime = LocalDateTime.now();
        BigDecimal annuity = planGeneratorService.calculateAnnuity(BigDecimal.valueOf(3000), 7.00, 18);
        List<BorrowerPayment> borrowerPayments = planGeneratorService.planGeneratorList(BigDecimal.valueOf(3000), 7.00, annuity, localDateTime, 18);
        Assert.assertEquals(borrowerPayments.get(17).getRemainingOutstandingPrincipal().doubleValue(), 0, 0d);
    }

    @Test
    public void endpointHttpResponseCode() throws Exception {
        LocalDateTime localDateTime = LocalDateTime.now();
        PlanGeneratorRequest planGeneratorRequest = new PlanGeneratorRequest(BigDecimal.valueOf(5000), 5, 24, localDateTime);
        mockMvc.perform(post("/generate-plan")
                .content(objectMapper.writeValueAsString(planGeneratorRequest))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void endpointResponseContentTypeTest() throws Exception {
        LocalDateTime localDateTime = LocalDateTime.now();
        PlanGeneratorRequest planGeneratorRequest = new PlanGeneratorRequest(BigDecimal.valueOf(5000), 5, 24, localDateTime);
        mockMvc.perform(post("/generate-plan")
                .content(objectMapper.writeValueAsString(planGeneratorRequest))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }


    @Test
    public void endpointResponseCollectionSizeTest() throws Exception {
        LocalDateTime localDateTime = LocalDateTime.now();
        PlanGeneratorRequest planGeneratorRequest = new PlanGeneratorRequest(BigDecimal.valueOf(3000), 7, 18, localDateTime);
        mockMvc.perform(post("/generate-plan")
                .content(objectMapper.writeValueAsString(planGeneratorRequest))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.borrowerPayments", hasSize(18)));
    }

    @Test
    public void endpointResponseCollectionFinalTest() throws Exception {
//        LocalDateTime localDateTime = LocalDateTime.now();
        LocalDateTime localDateTime = LocalDateTime.of(2020, Month.MAY, 22, 00, 0);
        localDateTime = localDateTime.truncatedTo(ChronoUnit.SECONDS);
        PlanGeneratorRequest planGeneratorRequest = new PlanGeneratorRequest(BigDecimal.valueOf(3000), 7, 18, localDateTime);
        mockMvc.perform(post("/generate-plan")
                .content(objectMapper.writeValueAsString(planGeneratorRequest))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.borrowerPayments[0].borrowerPaymentAmount", is(176.05)))
                .andExpect(jsonPath("$.borrowerPayments[0].initialOutstandingPrincipal", is(3000.00)))
                .andExpect(jsonPath("$.borrowerPayments[0].interest", is(17.50)))
                .andExpect(jsonPath("$.borrowerPayments[0].principal", is(158.55)))
                .andExpect(jsonPath("$.borrowerPayments[0].remainingOutstandingPrincipal", is(2841.45))).andExpect(jsonPath("$.borrowerPayments[0].borrowerPaymentAmount", is(176.05)))
                .andExpect(jsonPath("$.borrowerPayments[1].borrowerPaymentAmount", is(176.05)))
                .andExpect(jsonPath("$.borrowerPayments[1].initialOutstandingPrincipal", is(2841.45)))
                .andExpect(jsonPath("$.borrowerPayments[1].interest", is(16.58)))
                .andExpect(jsonPath("$.borrowerPayments[1].principal", is(159.47)))
                .andExpect(jsonPath("$.borrowerPayments[1].remainingOutstandingPrincipal", is(2681.98)))
        ;
    }
}

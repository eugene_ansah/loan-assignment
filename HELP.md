# Getting Started

### Run Project Locally
The project has an embedded server(Tomacat):

 1. Clone the project from repository (git clone git@bitbucket.org:eugene_ansah/loan-assignment.git)
 2. In your terminal Change current directory to project.
 3. TYpe  <code>./mvnw spring-boot:run</code> to run project

### Integration 
* The service is exposed on http://localhost:8085/generate-plan
* Send a post request to http://localhost:8085/generate-plan

### To Test plan generator service
1. <code>curl --location --request POST 'http://localhost:8085/generate-plan' \
--header 'Content-Type: application/json' \
--data-raw '{
    "loanAmount": 10000,
    "nominalRate": 7.0,
    "duration": 48,
    "startDate": "2019-01-01T00:00:01"
}'</code>
